# commander

A continuación, te presento algunos de los comandos más comunes en el sistema operativo Windows. Estos comandos se ejecutan en la línea de comandos o en el "Símbolo del sistema" (cmd.exe). Ten en cuenta que algunos comandos [destacados](https://destacados.org) pueden requerir privilegios de administrador para su ejecución.

1. **dir**: Muestra el contenido del directorio actual.

2. **cd**: Cambia el directorio actual. Por ejemplo, "cd C:\carpeta" te llevará a la carpeta llamada "carpeta" en el disco C.

3. **mkdir**: Crea un nuevo directorio. Por ejemplo, "mkdir NuevaCarpeta" creará una nueva carpeta llamada "NuevaCarpeta" en el directorio actual.

4. **rmdir**: Elimina un directorio vacío. Por ejemplo, "rmdir CarpetaVacia" eliminará la carpeta llamada "CarpetaVacia" si está vacía.

5. **del**: Elimina un archivo. Por ejemplo, "del archivo.txt" eliminará el archivo llamado "archivo.txt".

6. **copy**: Copia archivos. Por ejemplo, "copy archivo.txt C:\destino" copiará el archivo "archivo.txt" al directorio "destino" en el disco C.

7. **move**: Mueve archivos o cambia su nombre. Por ejemplo, "move archivo.txt C:\destino" moverá el archivo "archivo.txt" al directorio "destino" en el disco C.

8. **ren**: Cambia el nombre de un archivo o carpeta. Por ejemplo, "ren archivo.txt nuevo_nombre.txt" cambiará el nombre del archivo de "archivo.txt" a "nuevo_nombre.txt".

9. **tasklist**: Muestra una lista de los procesos en ejecución.

10. **taskkill**: Termina un proceso específico. Por ejemplo, "taskkill /IM nombre_proceso.exe" finalizará el proceso con el nombre "nombre_proceso.exe".

11. **ipconfig**: Muestra la configuración de red, incluida la dirección IP.

12. **ping**: Envía un paquete de prueba a una dirección IP para verificar la conectividad de red. Por ejemplo, "ping [fievent.com](https://fievent.com)" enviará paquetes de prueba al sitio web "fievent.com".

13. **shutdown**: Apaga o reinicia el sistema. Por ejemplo, "shutdown /s" apagará la computadora, mientras que "shutdown /r" reiniciará la computadora.

14. **gpupdate**: Actualiza la configuración de directivas de grupo.

15. **sfc**: Escanea y repara archivos del sistema corruptos. Por ejemplo, "sfc /scannow" realizará un escaneo en busca de archivos corruptos y tratará de repararlos.

Estos son solo algunos de los comandos más comunes en Windows. Hay muchos otros comandos disponibles que pueden ser útiles para tareas más específicas o avanzadas. Siempre ten cuidado al usar comandos en la línea de comandos, ya que algunos de ellos pueden afectar el sistema o los archivos de manera irreversible si se utilizan incorrectamente.
